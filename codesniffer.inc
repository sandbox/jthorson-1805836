<?php
/**
 * @file
 * Provide Drupal codesniffer integration.
 *
 */

/**
 * Number of files to be reviewed at the same time
 */
define('DRUPALCS_CHUNK', 30);

/**
 * Maximum number of relevant messages.
 */
define('DRUPALCS_MAX', 1000);

$plugin = array(
  'title' => t('Drupal codesniffer'),
  'description' => t('Provide Drupal codesniffer integration.'),
  'perform' => 'drupalcs_perform',
);

/**
 * Perform the job.
 *
 * @param $properties
 *   Associative array of properties defining the job.
 * @return
 *   An array containing a boolean for pass/fail and the result. The result is
 *   a multi-dimentional, associative array of coder_review results keyed by
 *   the file path and containing an array of messages with the keys: line,
 *   type, and message; and a sum of the number of messages of each type.
 *   The line key will FALSE if there is no line associated with the message.
 *
 *   @code
 *     $result = array(
 *       '#total' => array(1, 0, 0),
 *       'file1.php' => array(
 *         '#total' => array(1, 0, 0),
 *         array(
 *           'line' => 17, // number or FALSE
 *           'type' => 'major', // major|normal|minor
 *           'message' => 'This file is terrible.'
 *         ),
 *       ),
 *     );
 *   @endcode
 *
 * @see worker_perform()
 */
function drupalcs_perform(array $properties) {

  // TODO:  Add file extension types
  // TODO:  Add file filter support

  // Assemble list of relevant files
  worker_log('Assemble list of relevant files.');
  $files = worker_file_list($properties);
  $path = str_replace(DRUPAL_ROOT . '/', '', JOB_ROOT);
  array_walk($files, 'drupalcs_file_path', $path);

  worker_log('Found ' . count($files) . ' files.');

  // Set up the log filename where codesniffer results will be sent
  $log_file = JOB_ROOT . '/drupalcs_review.' . mt_rand(100, 10000);

  // Build the common part of the drupalcs command
  // TODO: --severity=
  $command = 'phpcs --standard=/home/www/.drush/coder/coder_sniffer/Drupal/ruleset.xml ';
  $command .= ' --report=csv ';
  // Split out extension to allow override via property in the future
  $command .= '--extensions=php,module,inc,install,test,profile,theme ';

  // Split the files into chunks that will be processed separately and build
  // the complete drush command.
  $i = 1;
  $chunks = array_chunk($files, DRUPALCS_CHUNK);
  $commands = array();
  foreach ($chunks as $chunk) {
    $commands['batch ' . $i++] = $command . implode(' ', $chunk) . ' >> ' . $log_file;
  }

  $log = worker_execute_concurrent($commands, 100);

  // Parse the result of the log file and collect the messages
  $map = array_flip(array('failure', 'error', 'warning'));
  $results = array('#total' => array(0, 0, 0));
  if (file_exists($log_file) && ($handle = fopen($log_file, "r")) !== FALSE) {
    while (($lines = fgetcsv($handle, 0, ',', '"')) !== FALSE) {
      if ($lines[1] == 'Line') {
        continue;
      }
      // Strip off the JOB_ROOT from the filename
      $file = str_replace(JOB_ROOT . '/', '', $lines[0]);
      if (!isset($results[$file])) {
        $results[$file]['#total'] = array(0,0,0);
      }
      // Update overall and per-file warning/error counts
      $type = $map[$lines[3]];
      $results['#total'][$type]++;
      $results[$file]['#total'][$type]++;
      // Update message information
      $results[$file][] = array(
        'line' => $lines[1],
        'type' => $lines[3],
        'message' => $lines[4],
      );

      // If the number of messages has reached the max then stop.
      if (array_sum($results['#total']) >= DRUPALCS_MAX) {
        worker_log('Reached maximum number (' . number_format(DRUPALCS_MAX) . ') of messages.');
        break;
      }
    }
    fclose($handle);
  }

  // If there are no critical or normal messages then consider the test a pass.
  return array($results['#total'][0] + $results['#total'][1] == 0, $results);
}

/**
 * Append the path to the file.
 *
 * @see array_walk()
 */
function drupalcs_file_path(&$file, $key, $path) {
  $file = $path . '/' . $file;
}
